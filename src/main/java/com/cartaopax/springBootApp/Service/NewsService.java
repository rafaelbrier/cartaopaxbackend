package com.cartaopax.springBootApp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cartaopax.springBootApp.Dao.NewsDao;
import com.cartaopax.springBootApp.Entity.Comment;
import com.cartaopax.springBootApp.Entity.News;

@Service
public class NewsService {

	@Autowired
	NewsDao newsDao;
	
	@Autowired
	CommentService commentService;
	
	public Page<News> findAll(String searchTerm, String category, Pageable pageable) {
		return this.newsDao.findAll(searchTerm, category, pageable);
	}
	
	public News findById(Integer id) {
		return this.newsDao.findById(id).orElse(null);
	}
	
	public Page<Comment> findCommentsByNews(Integer newsId, Pageable pageable) {
		return this.commentService.findByNewsId(newsId, pageable);
	}
	
	public News save(News news) {
		if(news.getId() != null && news.getId() > 0) {
			News newsEdit = this.findById(news.getId());
			
			if(news.getDate() != null) {
			} else {
				news.setDate(newsEdit.getDate());
			}
			
			news.setCommentCount(newsEdit.getCommentCount());
		}
		
		return this.newsDao.save(news);
	}
	
	public void delete(Integer id) {
		this.newsDao.deleteById(id);
	}
}
