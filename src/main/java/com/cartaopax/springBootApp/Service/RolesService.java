package com.cartaopax.springBootApp.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartaopax.springBootApp.Dao.RolesDao;
import com.cartaopax.springBootApp.Entity.Roles;

@Service
public class RolesService {

	@Autowired
	private RolesDao rolesDao;
	
	public Roles findById(Integer id) {
		return this.rolesDao.findFirstById(id);
	}
	
	public Long count() {
		return this.rolesDao.count();
	}
	
	public List<Roles> findAllOrderById() {
		return this.rolesDao.findAllByOrderById();
	}
}
