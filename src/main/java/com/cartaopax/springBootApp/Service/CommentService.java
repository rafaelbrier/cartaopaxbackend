package com.cartaopax.springBootApp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cartaopax.springBootApp.Dao.CommentDao;
import com.cartaopax.springBootApp.Entity.Comment;
import com.cartaopax.springBootApp.Entity.News;

@Service
public class CommentService {

	@Autowired
	CommentDao commentDao;
	
	@Autowired
	NewsService newsService;	
	
	public Page<Comment> findAll(String searchTerm, Pageable pageable) {
		return this.commentDao.findAll(searchTerm, pageable);
	}
	
	public Page<Comment> findByNewsId(Integer newsId, Pageable pageable) {
		return this.commentDao.findAllByNewsId(newsId, pageable);
	}
	
	public Comment findById(Integer id) {
		return this.commentDao.findById(id).orElse(null);
	}
	
	public Comment save(Integer newsId, Comment comment) {		
		News news = this.newsService.findById(newsId);
		news.setCommentCount(news.getCommentCount() + 1);
		comment.setNews(news);
		return this.commentDao.save(comment);		
	}
	
	public void delete(Integer id) {
		Comment comment = this.findById(id);
		News news = comment.getNews();
		news.setCommentCount(news.getCommentCount()-1);		
		this.commentDao.deleteById(id);
	}
}
