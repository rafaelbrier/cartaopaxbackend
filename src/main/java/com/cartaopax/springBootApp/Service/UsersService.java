package com.cartaopax.springBootApp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.cartaopax.springBootApp.Dao.UsersDao;
import com.cartaopax.springBootApp.Entity.Planos;
import com.cartaopax.springBootApp.Entity.Roles;
import com.cartaopax.springBootApp.Entity.Users;

@Service
public class UsersService {

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired 
	private RolesService rolesService;

	@Autowired 
	private PlanosService planosService;

	public Page<Users> findAll(String searchTerm, Boolean showDesactivated, Pageable pageable) {
		return this.usersDao.findAll(searchTerm, showDesactivated, pageable);
	}

	public Users save(Users user) {	

		String password = user.getBirthDate();
		Roles userRole = rolesService.findById(user.getRoles().getId());
		Planos userPlano = planosService.findById(user.getPlanos().getId());

		user.setRoles(userRole);
		user.setPlanos(userPlano);		

		//Encode BirthDate as Password
		user.setPassword(bCryptPasswordEncoder.encode(password));
		user.setNewsletter(true);
		user.setActive(true);

		Integer userAge = user.calculateAge();
		user.setAge(userAge);

		Double valor = user.calculatePlanPrice(user.getAge());		
		user.setPlanPrice(valor);

		return this.usersDao.save(user);

	}
	
	public Users findByCpf(String cpf) {
		return this.usersDao.findFirstByCpf(cpf);
	}

	public Users findById(Integer id) {
		return this.usersDao.findFirstById(id);
	}

	public void delete(Integer id) {
		this.usersDao.deleteById(id);
	}
	
	public void changeActiveState(Users user) {
		Boolean activeState = user.isActive();
		user.setActive(!activeState);
		this.usersDao.save(user);
	}

}
