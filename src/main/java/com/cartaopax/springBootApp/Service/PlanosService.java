package com.cartaopax.springBootApp.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartaopax.springBootApp.Dao.PlanosDao;
import com.cartaopax.springBootApp.Entity.Planos;
import com.cartaopax.springBootApp.plan.calculator.PlanCalculator;
import com.cartaopax.springBootApp.plan.calculator.PlanFactory;
import com.cartaopax.springBootApp.utils.DateUtils;

@Service
public class PlanosService {

	@Autowired
	private PlanosDao planosDao;

	public Planos findById(Integer id) {
		return this.planosDao.findFirstById(id);
	}

	public Long count() {
		return this.planosDao.count();
	}

	public List<Planos> findAllOrderById() {
		return this.planosDao.findAllByOrderById();
	}

	public Double calculatePlanPrice(String date, Integer planoId) {
		if(date != null && planoId != null) {
			LocalDate currentDate = LocalDate.now();
			LocalDate birthDate;

			if(DateUtils.isDateValid(date)) {
				birthDate = DateUtils.stringToDate(date);
			} else {
				return 0.00;
			}

			Integer Age = Period.between(birthDate, currentDate).getYears();

			PlanCalculator calculator = PlanFactory.getCalculatorByPlan(this.findById(planoId).getId());
			Integer ageRange = PlanFactory.findRangeByAge(Age);

			return calculator.calculatePrice(ageRange);
		} else {
			return 0.0;
		}
	}
}