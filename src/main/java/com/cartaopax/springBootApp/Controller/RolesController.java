package com.cartaopax.springBootApp.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartaopax.springBootApp.Entity.Roles;
import com.cartaopax.springBootApp.Service.RolesService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/roles")
public class RolesController {

	@Autowired
	private RolesService rolesService;
	
	@GetMapping()
	public List<Roles> findAllOrderById() {
		return this.rolesService.findAllOrderById();
	}
	
}
