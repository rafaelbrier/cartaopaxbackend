package com.cartaopax.springBootApp.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cartaopax.springBootApp.Entity.Planos;
import com.cartaopax.springBootApp.Service.PlanosService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/planos")
public class PlanosController {
	
	@Autowired
	private PlanosService planosService;
	
	@GetMapping()
	public List<Planos> findAllOrderById() {
		return this.planosService.findAllOrderById();
	}
	
	@GetMapping("/price")
	public Double findPlanPrice(@RequestParam(value="birthDate") String birthDate, @RequestParam(value="planoId") Integer planoId) {
		return this.planosService.calculatePlanPrice(birthDate, planoId);
	}

}
