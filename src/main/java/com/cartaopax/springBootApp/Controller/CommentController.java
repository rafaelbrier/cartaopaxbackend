package com.cartaopax.springBootApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cartaopax.springBootApp.Entity.Comment;
import com.cartaopax.springBootApp.Service.CommentService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/comments")
public class CommentController {
	
	@Autowired
	CommentService commentService;
	
	@GetMapping()
	public Page<Comment> findAll(@RequestParam(value="searchTerm", required=false) String searchTerm, Pageable pageable) {
		return this.commentService.findAll(searchTerm, pageable);
	}
	
	@GetMapping(path = "/{id}")
	public Comment findOne(@PathVariable Integer id) {
		return this.commentService.findById(id);
	}
	
	@DeleteMapping(path = "/{id}")
	public void delete(@PathVariable Integer id) {
		this.commentService.delete(id);
	}
}
