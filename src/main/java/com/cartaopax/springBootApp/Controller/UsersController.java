package com.cartaopax.springBootApp.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cartaopax.springBootApp.Entity.Users;
import com.cartaopax.springBootApp.Service.UsersService;
import com.cartaopax.springBootApp.utils.LoggedUserUtils;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UsersService usersService;
	
	@PreAuthorize("hasAnyAuthority('ADMIN','EMPLOYEE')")
	@GetMapping()
	public Page<Users> findAll(@RequestParam(value="searchTerm", required=false) String searchTerm, 
			@RequestParam(value="showDesactivated", required=false) Boolean showDesactivated,
			Pageable pageable) {
		return this.usersService.findAll(searchTerm, showDesactivated, pageable);
	}
	
	@PreAuthorize("this.checkIfIdMatches(#id) || hasAnyAuthority('ADMIN','EMPLOYEE')")
	@GetMapping(path = "/{id}")
	public Users findById(@PathVariable Integer id) {
		return this.usersService.findById(id);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN','EMPLOYEE')")
	@PostMapping(path = "/signup")	
	public ResponseEntity<String> signUp(@Valid @RequestBody Users user) 
	{
		if(user != null) {

			if(!user.getSex().equals("M") && !user.getSex().equals("F")) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("O Sexo só pode ser Masculino (M) ou Feminino (F).");
			}		

			try {
				this.usersService.save(user);
			}
			catch (DataIntegrityViolationException e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Já existe um usuário com CPF: " + user.getCpf());
			}
			catch (NullPointerException e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Informações inválidas, favor contatar o administrador do sistema.");
			}
			catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Erro ao efetuar a requisição, tente novamente mais tarde.");
			}
		}
		return null;
	}
	
	@PreAuthorize("this.checkIfIdMatches(#id)")
	@PostMapping(path = "/userEdit/{id}")	
	public ResponseEntity<String> userEdit(@Valid @RequestBody Users user, @PathVariable Integer id) 
	{
		if(user != null) {
			Users userToEdit = this.usersService.findById(user.getId());
			
			userToEdit.setName(user.getName());
			userToEdit.setEmail(user.getEmail());
			userToEdit.setTelephone(user.getTelephone());
			userToEdit.setCep(user.getCep());
			userToEdit.setImgProfile(user.getImgProfile());
			userToEdit.setCidade(user.getCidade());
			userToEdit.setBairro(user.getBairro());
			userToEdit.setEstado(user.getEstado());
			userToEdit.setNumero(user.getNumero());
			userToEdit.setComplemento(user.getComplemento());
			userToEdit.setEndereco(user.getEndereco());

			try {
				this.usersService.save(userToEdit);
			}
			catch (DataIntegrityViolationException e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Já existe um usuário com CPF: " + user.getCpf());
			}
		}
		return null;
	}
	
	public boolean checkIfIdMatches(Integer id) {
		Users user = LoggedUserUtils.getLoggedUser();
		if(user.getId() == id) {
			return true;
		} else {
			return false;
		}
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN','EMPLOYEE')")
	@PostMapping(path = "/actOrDesact")	
	public ResponseEntity<String> activateOrDesactivate(@RequestBody Integer userId) {
		Users user = this.usersService.findById(userId);
		if(user != null) {
			this.usersService.changeActiveState(user);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Usuário não encontrado.");
		}
		
		return null;
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN','EMPLOYEE')")
	@DeleteMapping(path = "/{id}")
	public void delete(@PathVariable Integer id) {
		this.usersService.delete(id);
	}
	
}
