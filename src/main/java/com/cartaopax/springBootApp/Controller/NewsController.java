package com.cartaopax.springBootApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cartaopax.springBootApp.Entity.Comment;
import com.cartaopax.springBootApp.Entity.News;
import com.cartaopax.springBootApp.Service.CommentService;
import com.cartaopax.springBootApp.Service.NewsService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/news")
public class NewsController {
	
	@Autowired
	NewsService newsService;
	
	@Autowired
	CommentService commentService;
	
	@GetMapping()
	public Page<News> findAll(@RequestParam(value="searchTerm", required=false) String searchTerm,
			@RequestParam(value="category", required=false) String category, Pageable pageable) {
		return this.newsService.findAll(searchTerm, category, pageable);
	}
			
	@GetMapping(path = "/{id}")
	public News findOne(@PathVariable Integer id) {
		return this.newsService.findById(id);
	}
	
	@GetMapping(path = "/{newsId}/comments")
	public Page<Comment> findCommentsByNews(@PathVariable Integer newsId, Pageable pageable) {
		return this.newsService.findCommentsByNews(newsId, pageable);
	}
	
	@PostMapping(path = "/{newsId}/comments")
	public Comment save(@PathVariable Integer newsId, @RequestBody Comment comment) {
		return this.commentService.save(newsId, comment);
	}
		
	@PostMapping
	public News save(@RequestBody News news) {
		return this.newsService.save(news);
	}
	
	@DeleteMapping(path = "/{id}")
	public void delete(@PathVariable Integer id) {
		this.newsService.delete(id);
	}
}
