package com.cartaopax.springBootApp.Entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

@Entity 	
public class News {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Integer id;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "date")
	@NotNull
	private LocalDateTime date;
	
	@Column(name = "commentCount")
	private int commentCount = 0;
	
	@NotNull
	@NotEmpty
	@Column(name = "title")
	@Size(max = 100)
	private String title;
	
	@Column(name = "img_path")
	@Size(max = 400)
	private String imgPath;
	
	@NotNull
	@NotEmpty
	@Column(name = "category")
	@Size(max = 10)
	private String category;
	
	@NotNull
	@NotEmpty
	@Lob 	
	@Column(name = "body", columnDefinition = "text")
	private String body;
	
	@JsonIgnore
	@OneToMany(mappedBy = "news", targetEntity = Comment.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<Comment> comments;
	
	@PrePersist
	public void setupDate() {
		this.date = LocalDateTime.now();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		if(category != null) {
			this.category = category;
		} else {
			this.category = "news";
		}
	}
	
}
