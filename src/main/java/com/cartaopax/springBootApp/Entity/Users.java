package com.cartaopax.springBootApp.Entity;

import java.beans.Transient;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.cartaopax.springBootApp.plan.calculator.PlanCalculator;
import com.cartaopax.springBootApp.plan.calculator.PlanFactory;
import com.cartaopax.springBootApp.utils.DateUtils;
import com.cartaopax.springBootApp.validation.BirthDate;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;


@Entity
public class Users implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Integer id;
	
	@NotEmpty(message = "O campo nome é obrigatório.")
	@NotNull(message = "O campo nome é obrigatório.")
	@Size(min= 4, max = 100, message= "O campo nome deve ter entre 4 a 100 caracteres.")
	@Column(name = "name")
	private String name;
	
	@Column(name = "img_profile")
	@Size(min= 20, max = 400, message= "A URL da imagem de perfil deve ter entre 20 a 400 caracteres.")
	private String imgProfile;
	
	@Column(name = "password")
	private String password;

	@NotEmpty(message = "O campo sexo é obrigatório.")
	@NotNull(message = "O campo sexo é obrigatório.")
	@Size(min=1, max = 2, message= "O campo sexo deve ter entre 1 e 2 caracteres.")
	@Column(name = "sex")
	private String sex;
	
	@NotEmpty(message = "O campo data de nascimento é obrigatório.")
	@NotNull(message = "O campo data de nascimento é obrigatório.")
	@BirthDate(message = "O campo data de nascimento é inválido.")
	@Size(min=10, max = 10, message= "O campo data de nascimento deve ter 10 caracteres.")
	@Column(name = "birth_date")
	private String birthDate;
	
	@NotEmpty(message = "O campo CPF é obrigatório.")
	@NotNull(message = "O campo CPF é obrigatório.")
	@CPF(message = "O CPF é inválido.")
	@Size(min=14, max=14, message= "O campo CPF deve ter 14 caracteres.")
	@Column(name = "cpf", unique=true)
	private String cpf;
	
	@NotEmpty(message = "O campo telefone é obrigatório.")
	@NotNull(message = "O campo telefone é obrigatório.")
	@Size(min=15, max = 15, message= "O campo telefone deve ter 15 caracteres.")
	@Column(name = "telephone")
	private String telephone;
	
	@Pattern(regexp="(^$|.{15,15})", message= "O campo telefone opcional deve ter 15 caracteres.")
	@Column(name = "telephone_op")
	private String telephoneOp;
	
	@Pattern(regexp="(^$|.{3,100})", message= "O campo email deve ter entre 6 a 100 caracteres.")
	@Email(message= "O email é inválido.")
	@Column(name = "email")
	private String email;
	
	@NotEmpty(message = "O campo escolaridade é obrigatório.")
	@NotNull(message = "O campo escolaridade é obrigatório.")
	@Size(min = 6, max = 50, message= "O campo escolaridade deve ter entre 6 a 50 caracteres.")
	@Column(name = "escolaridade")
	private String escolaridade;
	
	@Column(name = "age")
	private Integer age;
	
	@NotEmpty(message = "O campo CEP é obrigatório.")
	@NotNull(message = "O campo CEP é obrigatório.")
	@Size(min = 9, max = 9, message= "O campo CEP deve ter 9 caracteres.")
	@Column(name = "cep")
	private String cep;
	
	@NotEmpty(message = "O campo endereço é obrigatório.")
	@NotNull(message = "O campo endereço é obrigatório.")
	@Size(min = 6, max = 100, message= "O campo endereço deve ter entre 6 a 100 caracteres.")
	@Column(name = "endereco")
	private String endereco;
	
	@NotEmpty(message = "O campo número é obrigatório.")
	@NotNull(message = "O campo número é obrigatório.")
	@Size(min = 1, max = 6, message= "O campo número deve ter entre 1 a 6 caracteres.")
	@Column(name = "numero")
	private String numero;
	
	@Pattern(regexp="(^$|.{2,15})", message= "O campo complemento deve ter entre 2 a 15 caracteres.")
	@Column(name = "complemento")
	private String complemento;
	
	@NotEmpty(message = "O campo bairro é obrigatório.")
	@NotNull(message = "O campo bairro é obrigatório.")
	@Size(min=2, max = 50, message= "O campo bairro deve ter entre 1 a 50 caracteres.")
	@Column(name = "bairro")
	private String bairro;
	
	@NotEmpty(message = "O campo estado é obrigatório.")
	@NotNull(message = "O campo estado é obrigatório.")
	@Size(min=2, max = 50, message= "O campo estado deve ter entre 2 a 50 caracteres.")
	@Column(name = "estado")
	private String estado;
	
	@NotEmpty(message = "O campo cidade é obrigatório.")
	@NotNull(message = "O campo cidade é obrigatório.")
	@Size(min=2, max = 50, message= "O campo cidade deve ter entre 2 a 50 caracteres.")
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "newsletter")
	private boolean newsletter;

	@Column(name = "active")
    private boolean active;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "created_at")
	private LocalDateTime createdAt;
	
	@NotNull(message = "O campo Roles é obrigatório.")
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "roles_id", nullable = false)
	private Roles roles;
	
	@NotNull(message = "O campo Planos é obrigatório.")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "planos_id", nullable = false)
	private Planos planos;
	
	@NotNull(message = "O campo Preço do Plano é obrigatório.")
	@Column(name = "plan_price")
	private Double planPrice;
	
	
	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> profiles = new ArrayList<>();
		profiles.add(this.roles);
		return profiles;
	}
	
	@Transient
	public Double calculatePlanPrice(Integer userAge) {
		PlanCalculator calculator = PlanFactory.getCalculatorByPlan(planos.getId());
		Integer ageRange = PlanFactory.findRangeByAge(userAge);
		return calculator.calculatePrice(ageRange);
	}
	
	@Transient
	public int calculateAge() {
		LocalDate currentDate = LocalDate.now();
		LocalDate birthDate = DateUtils.stringToDate(this.getBirthDate());
		
		if ((birthDate != null) && (currentDate != null)) {
			return Period.between(birthDate, currentDate).getYears();
		} else {
			return 0;
		}
	}

	@Override
	public String getUsername() {
		return this.cpf;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String toString() {
		return this.id + " " + this.name + " " + this.birthDate;
	}
	
	@PrePersist
	public void setupCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgProfile() {
		return imgProfile;
	}

	public void setImgProfile(String imgProfile) {
		this.imgProfile = imgProfile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTelephone_op() {
		return telephoneOp;
	}

	public void setTelephone_op(String telephone_op) {
		this.telephoneOp = telephone_op;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}
	
	public Planos getPlanos() {
		return planos;
	}

	public void setPlanos(Planos planos) {
		this.planos = planos;
	}
	
	public Double getPlanPrice() {
		return planPrice;
	}

	public void setPlanPrice(Double planPrice) {
		this.planPrice = planPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTelephoneOp() {
		return telephoneOp;
	}

	public void setTelephoneOp(String telephoneOp) {
		this.telephoneOp = telephoneOp;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	
}
