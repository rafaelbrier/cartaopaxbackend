package com.cartaopax.springBootApp.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Planos {
	
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@Lob 	
	@Column(name = "descricao", columnDefinition = "text")
	private String descricao;
		
	@JsonIgnore
	@OneToMany(mappedBy = "planos", targetEntity = Users.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<Users> users;
	
	public Planos(Integer id, @NotNull String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Planos() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

}
