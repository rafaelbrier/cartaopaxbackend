package com.cartaopax.springBootApp.Entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Roles implements GrantedAuthority {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id", nullable = false)
    private Integer id;
	
	@NotNull
	@Size(max=20)
	@Column(name = "name")
	private String name;
    
	@NotNull
    @Column(name = "role")
    private String role;

    @JsonIgnore
	@OneToMany(mappedBy = "roles", targetEntity = Users.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<Users> users;

	public Roles() {
		super();
	}

	public Roles(Integer id, String name, String role) {
		this.id = id;
		this.name = name;
		this.role = role;
	}
	
	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String getAuthority() {
		return this.role;
	}
    
}