package com.cartaopax.springBootApp.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cartaopax.springBootApp.Entity.Comment;

public interface CommentDao extends CrudRepository<Comment, Integer> {
	
	@Query(value =  "SELECT * FROM Comment c WHERE"			
			+ " (c.news_id LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (c.author_name LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (c.author_email LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (c.body LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (DATE_FORMAT(c.date,'%d/%m/%Y %H:%s') LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)", nativeQuery  = true )
	public Page<Comment> findAll(@Param("searchTerm") String searchTerm, Pageable pageable);
	
	@Query(value = "select * from comment as c where c.news_id = :newsId", nativeQuery  = true )
	public Page<Comment> findAllByNewsId(@Param("newsId") Integer newsId, Pageable pageable);
	
}

