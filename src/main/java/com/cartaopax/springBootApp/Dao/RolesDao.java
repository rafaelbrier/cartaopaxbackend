package com.cartaopax.springBootApp.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cartaopax.springBootApp.Entity.Roles;

public interface RolesDao extends JpaRepository<Roles, Integer> {
	
    public Roles findFirstById(Integer id);
    
    public List<Roles> findAllByOrderById();

}