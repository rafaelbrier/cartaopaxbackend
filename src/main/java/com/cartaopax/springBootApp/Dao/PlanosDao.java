package com.cartaopax.springBootApp.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cartaopax.springBootApp.Entity.Planos;

public interface PlanosDao extends JpaRepository<Planos, Integer> {
	
    public Planos findFirstById(Integer id);
    
    public List<Planos> findAllByOrderById();

}