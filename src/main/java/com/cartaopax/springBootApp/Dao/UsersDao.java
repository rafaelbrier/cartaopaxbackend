package com.cartaopax.springBootApp.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cartaopax.springBootApp.Entity.Users;

public interface UsersDao extends CrudRepository<Users, Integer> {
	
	@Query(value = "SELECT * FROM Users u WHERE"			
			+ " ((u.name LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (u.cpf LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (DATE_FORMAT(u.birth_date,'%d/%m/%Y') LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL))"
			+ " AND (CASE WHEN (:showDesactivated)" 
			+ " THEN (u.active IS NOT NULL)"
			+ " ELSE (u.active = true) END)", nativeQuery = true)
	public Page<Users> findAll(
			@Param("searchTerm") String searchTerm, 
			@Param("showDesactivated") Boolean showDesactivated,
			Pageable pageable);
	
	public Users findFirstByCpf(String cpf);
	
	public Users findFirstById(Integer id);

}
