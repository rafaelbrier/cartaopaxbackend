package com.cartaopax.springBootApp.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cartaopax.springBootApp.Entity.News;

public interface NewsDao extends CrudRepository<News, Integer> {

	@Query(value = "SELECT * FROM News n WHERE"			
			+ " ((n.title LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (n.id LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL)"
			+ " OR (DATE_FORMAT(n.date,'%d/%m/%Y %Hh%i') LIKE CONCAT('%', :searchTerm, '%') OR :searchTerm IS NULL))"
			+ " AND (CASE WHEN (:category <> '')" 
			+ " THEN (n.category = :category)"
			+ " ELSE (n.category IS NOT NULL) END)", nativeQuery = true)
	public Page<News> findAll(
			@Param("searchTerm") String searchTerm, 
			@Param("category") String category,
			Pageable pageable);

}

