package com.cartaopax.springBootApp.auth.model;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class UserAuthenticationToken extends UsernamePasswordAuthenticationToken  {

	private static final long serialVersionUID = 1L;
	
	public UserAuthenticationToken(Object principal, Object credentials, Collection<GrantedAuthority> roles) {
		super(principal, credentials, roles);
	}


}
