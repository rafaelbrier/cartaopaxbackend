package com.cartaopax.springBootApp.auth.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.cartaopax.springBootApp.Entity.Roles;
import com.cartaopax.springBootApp.Entity.Users;
import com.cartaopax.springBootApp.Service.UsersService;
import com.cartaopax.springBootApp.auth.model.UserAuthenticationToken;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	UsersService userService;
	
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private String adminName = "Pax_Administrador";
	private String adminUsername = "cartaopaxadmin"; //adminUsername
	private String adminPassword = "123"; //adminPassword

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		//For Admin
		if(username.equals(adminUsername)) {			
			if(password.equals(adminPassword)) {
				
				Users adminUser = new Users();
				Roles adminRoles = new Roles();
				
				adminRoles.setRole("ADMIN");
				
				adminUser.setId(0);
				adminUser.setName(adminName);
				adminUser.setCpf(adminUsername);
				adminUser.setRoles(adminRoles);
				
				Authentication auth = new UserAuthenticationToken(adminUser.getUsername(), adminPassword, adminUser.getAuthorities());
		        ((UserAuthenticationToken)auth).setDetails(adminUser);
		        return auth;				
			}
		}
		
		Users user = userService.findByCpf(username);
		
		if(user != null) {
			
			boolean passwordMatches = bCryptPasswordEncoder.matches(password, user.getPassword()); 
			
			if(passwordMatches) {
				Authentication auth = new UserAuthenticationToken(user.getUsername(), password, user.getAuthorities());
		        ((UserAuthenticationToken)auth).setDetails(user);
		        return auth;
			}
		}
		
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
        return (UserAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
