package com.cartaopax.springBootApp.errorHandler;

public class ExceptionResponse {

    private String error;
    private Integer status;
    private String message;

    public ExceptionResponse() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
