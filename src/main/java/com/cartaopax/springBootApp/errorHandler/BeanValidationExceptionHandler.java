package com.cartaopax.springBootApp.errorHandler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class BeanValidationExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<BeanObjectError> errors = getErrors(ex);
		BeanValidatorErrorResponse errorResponse = getErrorResponse(ex, status, errors);
		return new ResponseEntity<>(errorResponse, status);
	}

	private BeanValidatorErrorResponse getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status,
			List<BeanObjectError> errors) {
		return new BeanValidatorErrorResponse("A requisição possui campos inválidos.", status.value(),
				status.getReasonPhrase(), ex.getBindingResult().getObjectName(), errors);
	}

	private List<BeanObjectError> getErrors(MethodArgumentNotValidException ex) {
		return ex.getBindingResult().getFieldErrors().stream().map(
				error -> new BeanObjectError(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
				.collect(Collectors.toList());
	}

}
