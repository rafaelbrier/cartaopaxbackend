package com.cartaopax.springBootApp.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import com.cartaopax.springBootApp.auth.provider.UserAuthenticationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	protected UserAuthenticationProvider userAuthenticationProvider;

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());

		httpSecurity.csrf().disable().authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
		.antMatchers("/check").permitAll()
		.antMatchers("/comments/**").permitAll()

		.antMatchers(HttpMethod.GET,"/planos/**").permitAll()

		.antMatchers(HttpMethod.GET,"/news/**").permitAll()
		.antMatchers(HttpMethod.POST,"/news/**/comments").permitAll()
		.antMatchers(HttpMethod.POST,"/news/**").hasAnyAuthority("ADMIN","EMPLOYEE")
		.antMatchers(HttpMethod.DELETE,"/news/**").hasAnyAuthority("ADMIN","EMPLOYEE")

//		.antMatchers(HttpMethod.GET,"/users/**").hasAnyAuthority("ADMIN","EMPLOYEE")
		
		.antMatchers(HttpMethod.GET,"/roles/**").hasAnyAuthority("ADMIN","EMPLOYEE")
//		.antMatchers(HttpMethod.GET,"/planos/").hasAnyAuthority("ADMIN","EMPLOYEE","CLIENT")
		
		.antMatchers(HttpMethod.POST,"/users/signup/**").hasAnyAuthority("ADMIN","EMPLOYEE")

		.antMatchers(HttpMethod.POST, "/login").permitAll()

		.anyRequest().authenticated()
		.and()

		// filtra requisições de login
		.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
				UsernamePasswordAuthenticationFilter.class)
		.authenticationProvider(userAuthenticationProvider)

		// filtra outras requisições para verificar a presença do JWT no header
		.addFilterBefore(new JWTAuthenticationFilter(),
				UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(userAuthenticationProvider);
	}

}