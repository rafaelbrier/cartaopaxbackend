package com.cartaopax.springBootApp.Security;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.cartaopax.springBootApp.Entity.Roles;
import com.cartaopax.springBootApp.Entity.Users;
import com.cartaopax.springBootApp.auth.model.UserAuthenticationToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {

	// EXPIRATION_TIME = 10 dias
	static final long EXPIRATION_TIME = 860_000_000;
	static final String SECRET = "CartaoPaxSecretKey_Brier";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";

	static void addAuthentication(HttpServletResponse response, Users user) {
		
		Map<String, Object> body = new HashMap<>();
		body.put("id", user.getId());
		body.put("name", user.getName());
		body.put("cpf", user.getCpf());
		body.put("imgProfile", user.getImgProfile());
		body.put("sex", user.getSex());
		body.put("role", user.getAuthorities().iterator().next().getAuthority());
		
		String JWT = Jwts.builder()
				.setClaims(body)
				.setSubject(user.getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET)
				.compact();
		
			Date expirationTimeDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
			String token = "\""+HEADER_STRING+"\"" + ": " + "\"" + TOKEN_PREFIX + " " + JWT + "\"";
			String expirationTime = "\"expirationTime\": " + "\"" +
					new SimpleDateFormat("yyyy-MM-dd HH:mm").format(expirationTimeDate)+ "\"";
	
			try {
				PrintWriter out = response.getWriter();
				out.println("{" + token + "," + expirationTime + "}");
				response.flushBuffer();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);

		if (token != null) {
			// faz parse do token
			Claims claims = Jwts.parser()
					.setSigningKey(SECRET)
					.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody(); 
			
			String user = claims.getSubject();
			
			if (user != null) {
				
				List<GrantedAuthority> authorities = new ArrayList<>();
				Roles roles = new Roles();
				roles.setRole(claims.get("role").toString());			
				authorities.add(roles);

				Users users = new Users();
				users.setId((Integer) claims.get("id"));
				users.setCpf(claims.get("cpf").toString());
				users.setName(claims.get("name").toString());
				
				UserAuthenticationToken auth = new UserAuthenticationToken(user, null, authorities);
				auth.setDetails(users);
				return auth;
			}
		}
		return null;
	}

}