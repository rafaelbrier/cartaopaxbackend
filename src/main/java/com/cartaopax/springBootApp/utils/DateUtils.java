package com.cartaopax.springBootApp.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class DateUtils {

	public static final LocalDate stringToDate(String strDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			return LocalDate.parse(strDate, formatter);
		} catch(Exception e) {
			return null;
		}
	}

	public static final boolean isDateValid(String strDate) {
		return stringToDate(strDate) != null;
	}
	
	public static final boolean isBirthDateValid(String strDate) {
		if(!isDateValid(strDate)) {
			return false;
		}
		LocalDate birthDate = stringToDate(strDate);
		LocalDate currentDate = LocalDate.now();
		Integer datesComparedResult = birthDate.compareTo(currentDate);
		
		if(datesComparedResult < 0) {
			return true;
		} else {
			return false;
		}
	}

}
