package com.cartaopax.springBootApp.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.cartaopax.springBootApp.Entity.Users;

public class LoggedUserUtils {

	public static Users getLoggedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return (Users) auth.getDetails();
	}
}
