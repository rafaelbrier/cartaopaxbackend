package com.cartaopax.springBootApp.plan.calculator;

public class PlanCompletoCalculator implements PlanCalculator {
	
	private static final Double[] PlanPrice = {100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0};
	
	public Double calculatePrice(Integer ageRange) {
		
		return PlanPrice[ageRange];
	}
	
}
