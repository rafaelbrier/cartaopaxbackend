package com.cartaopax.springBootApp.plan.calculator;

public class PlanCooparticipativoCalculator implements PlanCalculator {
	
	private static final Double[] PlanPrice = {50.0, 100.0, 150.0, 200.0, 250.0, 300.0, 350.0};

	public Double calculatePrice(Integer ageRange) {
		
		return PlanPrice[ageRange];
	}
}
