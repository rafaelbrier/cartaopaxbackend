package com.cartaopax.springBootApp.plan.calculator;

public class PlanEnfermariaCalculator implements PlanCalculator {
	
	private static final Double[] PlanPrice = {10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0};

	public Double calculatePrice(Integer ageRange) {
		return PlanPrice[ageRange];
	}
}
