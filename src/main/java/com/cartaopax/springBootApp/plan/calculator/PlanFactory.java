package com.cartaopax.springBootApp.plan.calculator;

public enum PlanFactory {

	PLAN_COMPLETO(1, new PlanCompletoCalculator()),
	PLAN_COOPARTICIPATIVO(2, new PlanCooparticipativoCalculator()),
	PLAN_ENFERMARIA(3, new PlanEnfermariaCalculator());

	private int id;
	private PlanCalculator calculator;

	private PlanFactory(int id, PlanCalculator calculator) {
		this.id = id;
		this.calculator = calculator;
	}

	public static PlanCalculator getCalculatorByPlan(int planId) {
		for(PlanFactory plan : values()) {
			if(plan.id == planId) {
				return plan.calculator;
			}
		}
		return null;
	}

	public static Integer findRangeByAge(Integer userAge) {
		if(userAge >= 0 && userAge < 10) {
			return 0;
		} else if (userAge >= 10 && userAge < 20) {
			return 1;
		} else if (userAge >= 20 && userAge < 30) {
			return 2;
		} else if (userAge >= 30 && userAge < 40) {
			return 3;
		} else if (userAge >= 40 && userAge < 50) {
			return 4;
		} else if (userAge >= 50 && userAge < 60) {
			return 5;
		} else if (userAge >= 60 ) {
			return 6;
		} 
		throw new NullPointerException("Couldn't set a user Age Range!");
	}
}
