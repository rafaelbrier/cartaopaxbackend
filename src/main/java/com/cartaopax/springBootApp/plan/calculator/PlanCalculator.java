package com.cartaopax.springBootApp.plan.calculator;

public interface PlanCalculator {

	public Double calculatePrice(Integer ageRange);
}
