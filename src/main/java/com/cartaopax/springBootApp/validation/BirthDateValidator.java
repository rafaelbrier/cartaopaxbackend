package com.cartaopax.springBootApp.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cartaopax.springBootApp.utils.DateUtils;

public class BirthDateValidator implements ConstraintValidator<BirthDate, String> {

	String mensagemErro;
	
	public static boolean isBirthDateValid(String birthDate) {
		return DateUtils.isBirthDateValid(birthDate);
	}
	
	@Override
    public void initialize(BirthDate constraintAnnotation) {
		this.mensagemErro = constraintAnnotation.message();
    }

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
        context
            .buildConstraintViolationWithTemplate(this.mensagemErro)
            .addConstraintViolation();
		return isBirthDateValid(value);
	}

}
