package com.cartaopax.springBootApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CartaoPaxBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaoPaxBackendApplication.class, args);
	}

	@RequestMapping("/check")
	public String check() {
		return "The server is running fine! Thank you!";
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

