package com.cartaopax.springBootApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.cartaopax.springBootApp.Dao.PlanosDao;
import com.cartaopax.springBootApp.Dao.RolesDao;
import com.cartaopax.springBootApp.Entity.Planos;
import com.cartaopax.springBootApp.Entity.Roles;

@Component
public class DataLoader implements ApplicationRunner {

	private RolesDao rolesDao;
	private PlanosDao planosDao;

	@Autowired
	public DataLoader(RolesDao rolesDao, PlanosDao planosDao) {
		this.rolesDao = rolesDao;
		this.planosDao = planosDao;
	}

	public void run(ApplicationArguments args) {
			Roles roleAdmin = new Roles(1, "ADMINISTRADOR", "ADMIN");
			Roles roleEmployee = new Roles(2, "FUNCIONÁRIO", "EMPLOYEE");
			Roles roleClient = new Roles(3, "CLIENTE", "CLIENT");

			rolesDao.save(roleAdmin);
			rolesDao.save(roleEmployee);
			rolesDao.save(roleClient);			
			
			Planos planoCompleto = new Planos((Integer) 1, "PLANO COMPLETO");
			Planos planoCooparticipativo = new Planos(2, "PLANO COOPARTICIPATIVO");
			Planos PlanoEnfermaria = new Planos(3, "PLANO ENFERMARIA");

			planosDao.save(planoCompleto);
			planosDao.save(planoCooparticipativo);
			planosDao.save(PlanoEnfermaria);			
		
				
	}
}